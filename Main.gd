extends Node


var level:= 0


func _ready() -> void:
	var _back_pressed = $GUI.connect("back_pressed", self, "_on_GUI_back_pressed")
	var _btn_spin_pressed = $GUI.connect("btn_spin_pressed", self, "_on_GUI_btn_spin_pressed")
	var _btn_autospin_pressed = $GUI.connect("btn_autospin_pressed", self, "_on_GUI_btn_autospin_pressed")

	var _button_slot_pressed = $Menu.connect("button_slot_pressed", self, "_on_Menu_button_slot_pressed")
	var _button_wheel_pressed = $Menu.connect("button_wheel_pressed", self, "_on_Menu_button_wheel_pressed")

	var _wheel_score_increased = $GameWheel.connect("wheel_score_increased", self, "_on_GameWheel_wheel_score_increased")
	var _slot_score_increased = $GameSlot.connect("slot_score_increased", self, "_on_GameSlot_slot_score_increased")
	var _spinnig_changed = $GameSlot.connect("spinnig_changed", self, "_on_GameSlot_spinnig_changed")
	var _auto_spinnig_changed = $GameSlot.connect("auto_spinnig_changed", self, "_on_GameSlot_auto_spinnig_changed")
	var _autospin_count_changed = $GameSlot.connect("autospin_count_changed", self, "_on_GameSlot_autospin_count_changed")

	$GUI.show_hide_controls(false, false)
	$Menu.show_hide_menu(true)
	$GameWheel.show_hide_game(false)
	$GameSlot.show_hide_game(false)


func _on_GUI_back_pressed() -> void:
	$GUI.show_hide_controls(false, false)
	$Menu.show_hide_menu(true)
	$GameWheel.show_hide_game(false)
	$GameSlot.show_hide_game(false)


func _on_GUI_btn_spin_pressed() -> void:
	$GameSlot.spin()


func _on_GUI_btn_autospin_pressed() -> void:
	$GameSlot.auto_spin()


func _on_Menu_button_slot_pressed(game_level: int) -> void:
	level = game_level
	$GUI.show_hide_controls(true, true)
	$Menu.show_hide_menu(false)
	$GameWheel.show_hide_game(false)
	$GameSlot.load_level(game_level)
	$GameSlot.show_hide_game(true)


func _on_Menu_button_wheel_pressed() -> void:
	$GUI.show_hide_controls(true, false)
	$Menu.show_hide_menu(false)
	$GameWheel.show_hide_game(true)
	$GameSlot.show_hide_game(false)


func _on_GameWheel_wheel_score_increased(score_offset: int) -> void:
	$GUI.set_wheel_score(score_offset)


func _on_GameSlot_slot_score_increased() -> void:
	$GUI.set_slot_score()


func _on_GameSlot_spinnig_changed(status: bool) -> void:
	$GUI.spinning = status


func _on_GameSlot_auto_spinnig_changed(status: bool) -> void:
	$GUI.auto_spinning = status
	if not status:
		$GUI.show_hide_autospin_progress(false)


func _on_GameSlot_autospin_count_changed(auto_spin_count: float, auto_spin_max: float) -> void:
	$GUI.change_autospin_count(auto_spin_count, auto_spin_max)
