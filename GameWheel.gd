extends Control


signal wheel_score_increased


onready var wheel:TextureRect = get_node("Wheel")
onready var tween = get_node("Tween")


var is_active = false

export var sections_count:float = 18
var section_size:float = 360 / sections_count
const SECTION_PRICES:= [
	1000000.0,
	10.0,
	20.0,
	1000.0,
	5.0,
	30.0,
	5200.0,
	50.0,
	100.0,
	5000.0,
	10.0,
	50000.0,
	1000.0,
	0.0,
	200.0,
	5.0,
	100.0,
	1.0
]
onready var angle_offset:float = wheel.rect_rotation

var speed = 0
const START_SPEED:float = 1300.0
const SPEED_DRAG:float = 5.0


func _process(delta: float) -> void:
	if speed != 0:
		wheel.rect_rotation += delta * speed
		speed = speed - SPEED_DRAG if speed > 0 else 0
	else:
		if fmod(wheel.rect_rotation, section_size) != 0:
			_move_wheel()


func _move_wheel() -> void:
	var align_rotation: int
	var wheel_rotation:= fmod(wheel.rect_rotation, 360)
	var wheel_rotaion_offset:= fmod(wheel_rotation, section_size)

	if wheel_rotaion_offset > section_size / 2:
		align_rotation = round(wheel_rotation + (section_size - wheel_rotaion_offset) + angle_offset) as int
	else:
		align_rotation = round(wheel_rotation - wheel_rotaion_offset + angle_offset) as int
	if is_active:
		tween.interpolate_property(wheel, "rect_rotation", wheel_rotation, align_rotation, 0.1)
		if not tween.is_active():
			tween.start()
		_set_score(align_rotation)


func _set_score(wheel_angle: int) -> void:
	var prise_index = round(wheel_angle / section_size)
	if prise_index >= SECTION_PRICES.size():
		prise_index = 0
	var score = SECTION_PRICES[prise_index]
	is_active = false
	emit_signal("wheel_score_increased", score)


func _on_BtnSpinWheel_pressed() -> void:
	if speed == 0:
		speed = START_SPEED
		is_active = true


func show_hide_game(show: bool) -> void:
	if show:
		self.show()
	else:
		self.hide()
