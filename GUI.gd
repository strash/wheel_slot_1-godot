extends Control


export (PackedScene) var Counter

signal back_pressed
signal btn_spin_pressed
signal btn_autospin_pressed


# меняется сигналами из GameSlot через Main
var spinning = false
var auto_spinning = false


var score:= 10000.0
var level:= 1 # уровень на шкале уровня
var level_score:= 0.0 # выигранные очки
var level_step:= 200 # шаг уровня
var chanse_to_win:= 15 # шанс на победу. условно в процентах
var rate:= 15.0 # повышающий коэффициент при выигрыше

var min_bet:= 0.25 # минимальный порог ставки
var max_bet:= 5.0 # максимальный порог ставки
var bet_step:= 0.25 # шаг инкремента/дикремента
var bet:= 2.5 # текущий коэффициент ставки



func _ready() -> void:
	set_level(0.0)
	show_hide_autospin_progress(false)

func _process(_delta: float) -> void:
	$Header/Score/Label.text = "$ %s" % str(round(score * 100) / 100)
	$Footer/Bet/Label.text = "BET: $ %s" % str(round(bet * 100) / 100)


func show_hide_controls(show: bool, footer: bool) -> void:
	if show:
		$Header/BtnBack.show()
		$Footer/Bet.show()
		$Footer/BtnAutoSpin.show()
		$Footer/BtnSpin.show()
		$BG.hide()
	else:
		$Header/BtnBack.hide()
		$Footer/Bet.hide()
		$Footer/BtnAutoSpin.hide()
		$Footer/BtnSpin.hide()
		$BG.show()
	if footer:
		$Footer.show()
	else:
		$Footer.hide()


func set_wheel_score(score_offset: int) -> void:
	$Tween.interpolate_property(self, "score", score, score + score_offset, 0.5)
	if not $Tween.is_active():
		$Tween.start()


func set_slot_score() -> void:
	var counter = Counter.instance()
	if randi() % 100 <= chanse_to_win + chanse_to_win * bet / 2:
		var count = floor(rate * bet + level)
		$Tween.interpolate_property(self, "score", score, score + count, 0.5)
		set_level(count)
		counter.increase = true
		counter.count = count
	else:
		$Tween.interpolate_property(self, "score", score, score - bet, 0.5)
		counter.increase = false
		counter.count = bet

	get_parent().add_child(counter)
	if not $Tween.is_active():
		$Tween.start()


# установка уровня
func set_level(count: float) -> void:
	level_score = level_score + count
	if level_score > level * level_step:
		level_score = level_score - level * level_step
		level += 1
	$Header/Level/Star/Label.text = str(level)
	$Header/Level/Label.text = str(level_score) + "/" + str(level * level_step)
	var progress = lerp(40, 291, level_score / (level * level_step))
	$Tween.interpolate_property($Header/Level/Progress, "rect_size:x", $Header/Level/Progress.rect_size.x, progress, 0.2)
	var opacity = 1.0
	if level_score <= level * level_step / 10.0:
		opacity = level_score / (level * level_step / 10.0)
	$Tween.interpolate_property($Header/Level/Progress, "modulate:a", $Header/Level/Progress.modulate.a, opacity, 0.2)
	if not $Tween.is_active():
		$Tween.start()



func _on_BtnBack_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("back_pressed")


func _on_BtnSpin_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_spin_pressed")


func _on_BtnAutoSpin_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)


func show_hide_autospin_progress(show: bool) -> void:
	if show:
		$Tween.interpolate_property($Footer/AutoSpinProgress, "rect_position:y", 200, 60, 0.3)
		$Tween.interpolate_property($Footer/BtnAutoSpin, "rect_position:y", 42, 200, 0.3)
		$Tween.interpolate_property($Footer/BtnSpin, "rect_position:y", 42, 200, 0.3)
	else:
		$Tween.interpolate_property($Footer/AutoSpinProgress, "rect_position:y", 60, 200, 0.3)
		$Tween.interpolate_property($Footer/BtnAutoSpin, "rect_position:y", 200, 42, 0.3)
		$Tween.interpolate_property($Footer/BtnSpin, "rect_position:y", 200, 42, 0.3)
	if not $Tween.is_active():
		$Tween.start()
	$Footer/AutoSpinProgress/Progress.rect_size.x = 46.6


func change_autospin_count(auto_spin_count: float, auto_spin_max: float) -> void:
	$Footer/AutoSpinProgress/Label.text = "%s / %s" % [auto_spin_count, auto_spin_max]
	var progress:float = lerp(40, 466, auto_spin_count / auto_spin_max)
	var x:float = $Footer/AutoSpinProgress/Progress.rect_size.x
	$Tween.interpolate_property($Footer/AutoSpinProgress/Progress, "rect_size:x", x, progress, 0.2)
	if not $Tween.is_active():
		$Tween.start()


func _on_BtnMinus_pressed() -> void:
	if not spinning and not auto_spinning:
		var old_bet = bet
		bet = bet - bet_step if bet > min_bet else min_bet
		$Tween.interpolate_property(self, "bet", old_bet, bet, 0.05)

		var btn_min_opacity = $Footer/Bet/BtnMinus.modulate.a
		var btn_max_opacity = $Footer/Bet/BtnPlus.modulate.a
		if bet == min_bet:
			$Tween.interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 0.0, 0.4)
		else:
			$Tween.interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, 0.4)
		$Tween.interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, 0.4)

		if not $Tween.is_active():
			$Tween.start()


func _on_BtnPlus_pressed() -> void:
	if not spinning and not auto_spinning:
		var old_bet = bet
		bet = bet + bet_step if bet < max_bet else max_bet
		$Tween.interpolate_property(self, "bet", old_bet, bet, 0.1)

		var btn_max_opacity = $Footer/Bet/BtnPlus.modulate.a
		var btn_min_opacity = $Footer/Bet/BtnMinus.modulate.a
		if bet == max_bet:
			$Tween.interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 0.0, 0.4)
		else:
			$Tween.interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, 0.4)
		$Tween.interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, 0.4)

		if not $Tween.is_active():
			$Tween.start()
