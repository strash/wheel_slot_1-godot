extends Control


export (PackedScene) var Icon


signal slot_score_increased
signal spinnig_changed
signal auto_spinnig_changed
signal autospin_count_changed


var lvl = [
	# level 1
	{
		# ширина и высота иконки
		ic_w = 184,
		ic_h = 156,
		# отступы между иконками
		ic_gap_w = 2,
		ic_gap_h = 2,
		# сдвиг иконок внутри доски. если null, то центрируется из центра
		ic_offset_x = null,
		ic_offset_y = null,
		# сдвиг доски внутри экрана. если null, то центрируется из центра
		brd_offset_x = null,
		brd_offset_y = 80,
		# количество иконок горизонтально и вертикально
		ic_count_x = 5,
		ic_count_y = 3,
		ic_variations = 6 # количество иконок в сете
		# preloaded_textures = [Texture, ...] — при инициализации подгружаются текстуры
	},
	# level 2
	{
		ic_w = 200,
		ic_h = 150,
		ic_gap_w = 68,
		ic_gap_h = 5,
		ic_offset_x = null,
		ic_offset_y = 24,
		brd_offset_x = null,
		brd_offset_y = 80,
		ic_count_x = 3,
		ic_count_y = 3,
		ic_variations = 6
	},
	# level 3
	{
		ic_w = 200,
		ic_h = 155,
		ic_gap_w = 8,
		ic_gap_h = 8,
		ic_offset_x = null,
		ic_offset_y = 45,
		brd_offset_x = null,
		brd_offset_y = 60,
		ic_count_x = 3,
		ic_count_y = 3,
		ic_variations = 9
	}
]


var level:= 1

var spin_time_max:= 4.0 # максимальное время кручения спина
var spin_timer:= 4.0 # таймер кручения спина
var spin_speed:= 100.0 # скорость спина

var spinning:= false setget set_spinning, get_spinning # состояние спина
var auto_spinning:= false setget set_auto_spinning, get_auto_spinning # состояние автоспина
var auto_spin_max:= 10 # количество повторов автоспина
var auto_spin_count:= 1 # счетчик количества повторов автоспина


func _ready() -> void:
	randomize()
	var path: String
	# кеширование всех текстур
	for i in lvl.size():
		lvl[i].preloaded_textures = []
		for j in lvl[i].ic_variations:
			path = "res://assets/image/ic_%s_%s.png" % [ str(i + 1), str(j + 1) ]
			lvl[i].preloaded_textures.append(load(path))
	emit_signal("autospin_count_changed", auto_spin_count as float, auto_spin_max as float)


func _process(delta: float) -> void:
	if get_spinning():
		spin_timer += spin_speed * delta
		if spin_timer > spin_time_max:
			_spin_play()
			spin_timer = 0



# setget spinning
func set_spinning(status: bool) -> void:
	spinning = status
	emit_signal("spinnig_changed", status)

func get_spinning() -> bool:
	return spinning

# setget auto_spinning
func set_auto_spinning(status: bool) -> void:
	auto_spinning = status
	emit_signal("auto_spinnig_changed", status)

func get_auto_spinning() -> bool:
	return auto_spinning



func show_hide_game(show: bool) -> void:
	if show:
		self.show()
	else:
		self.hide()


func load_level(loaded_level) -> void:
	level = loaded_level - 1
	for icon in get_tree().get_nodes_in_group("icons_group"):
		icon.queue_free()
	show_level()
	set_slot_grid()
	set_icons()


# отображение выбранного уровня
func show_level() -> void:
	$Bg.set_texture( load("res://assets/image/bg_%s.png" % str(level + 1)) )
	$Board.set_texture( load("res://assets/image/board_%s.png" % str(level + 1)) )
	$Board.rect_size = Vector2(0.0, 0.0) # HACK: чтобы сбрасывать размер контейнера после изменения текстуры
	$Board.rect_position = (get_viewport_rect().size - $Board.rect_size) / 2
	if lvl[level].brd_offset_x != null:
		$Board.rect_position.x = lvl[level].brd_offset_x
	if lvl[level].brd_offset_y != null:
		$Board.rect_position.y = lvl[level].brd_offset_y
	spin_timer = spin_time_max


# создание сетки слотов. l — таблица с параметрами текущего уровня
func set_slot_grid() -> void:
	var l = lvl[level]
	var size = $Board.rect_size
	var o_x: float
	var o_y: float
	# если не задан оффсет, то сетка по центру
	o_x = (size.x - (l.ic_w * l.ic_count_x + l.ic_gap_w * (l.ic_count_x - 1))) / 2 if l.ic_offset_x == null else l.ic_offset_x
	o_y = (size.y - (l.ic_h * l.ic_count_y + l.ic_gap_h * (l.ic_count_y - 1))) / 2 if l.ic_offset_y == null else l.ic_offset_y
	# отрисовка сетки
	for i in l.ic_count_x:
		var p_x = o_x + (l.ic_w + l.ic_gap_w) * i + 1
		for j in l.ic_count_y:
			var p_y = o_y + (l.ic_h + l.ic_gap_h) * j + 1
			var child = Icon.instance()
			child.rect_position = Vector2(p_x, p_y)
			$Board.add_child(child)


# уставнока слотов (иконок)
func set_icons() -> void:
	var ic_texture: Object
	var vars: int = lvl[level].ic_variations
	for icon in get_tree().get_nodes_in_group("icons_group"):
		ic_texture = lvl[level].preloaded_textures[randi() % vars]
		icon.set_texture(ic_texture)


# вызов начала спина
func spin() -> void:
	if not get_spinning() and not get_auto_spinning():
		for slot in get_tree().get_nodes_in_group("icons_group"):
			slot.set_blur(true)
		$TimerSpin.start()
		set_spinning(true)
		_spin_play()


# вызов начала автоспина
func auto_spin() -> void:
	if not get_spinning() and not get_auto_spinning():
		for slot in get_tree().get_nodes_in_group("icons_group"):
			slot.set_blur(true)
		$TimerSpin.start()
		set_spinning(true)
		set_auto_spinning(true)
		_spin_play()


# запуск спина. повторяется
func _spin_play() -> void:
	set_icons()


# остановка спина. повторяется
func _spin_stop() -> void:
	set_spinning(false)
	for slot in get_tree().get_nodes_in_group("icons_group"):
		slot.set_blur(false)
	if get_auto_spinning():
		$TimerSpinPause.start()



# сигнал таймера кручения
func _on_TimerSpin_timeout() -> void:
	if get_spinning():
		_spin_stop()
		emit_signal("slot_score_increased")


# сигнал таймера паузы. перезапускает спин автоматически
func _on_TimerSpinPause_timeout() -> void:
	if auto_spin_max > auto_spin_count: # если меньше максимального количества спинов, то продолжаем крутить
		auto_spin_count += 1 # увеличиваем счетчик
		set_spinning(true)
		for slot in get_tree().get_nodes_in_group("icons_group"):
			slot.set_blur(true)
			$TimerSpin.start()
	else: # если закончили, то останавливаем автоспины
		set_spinning(false)
		set_auto_spinning(false)
		auto_spin_count = 1
	emit_signal("autospin_count_changed", auto_spin_count as float, auto_spin_max as float)
	spin_timer = 0
