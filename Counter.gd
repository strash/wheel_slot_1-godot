extends Label


var increase = false
var count = 0

var green = Color(0.0 / 255.0, 175.0 / 255.0, 0.0 / 255.0)
var red = Color(185.0 / 255.0, 0.0 / 255.0, 122.0 / 255.0)



func _ready() -> void:
	var pos = self.rect_position
	var new_pos: Vector2
	var new_pos_x = rand_range(pos.x - 20.0, pos.x + 20.0)
	if increase:
		self.text = "$ +%s" % str(count)
		self.set("custom_colors/font_color", green)
		new_pos = Vector2(new_pos_x, rand_range(pos.y - 150.0, pos.y - 100.0))
	else:
		self.text = "$ -%s" % str(count)
		self.set("custom_colors/font_color", red)
		new_pos = Vector2(new_pos_x, rand_range(pos.y + 100.0, pos.y + 150.0))
	$Tween.interpolate_property(self, "rect_position", pos, new_pos, 1.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.start()
	$CPUParticles2D.emitting = true
	yield(get_tree().create_timer(1.2), "timeout")
	$Tween.interpolate_property(self, "modulate:a", 1.0, 0.0, 0.3)
	$Tween.start()
	yield(get_tree().create_timer(0.3), "timeout")
	queue_free()



# func _on_Timer_timeout() -> void:
# 	queue_free()
