extends Control


signal button_slot_pressed
signal button_wheel_pressed


func show_hide_menu(show: bool) -> void:
	if show:
		self.show()
	else:
		self.hide()


func _on_Btn_1_pressed() -> void:
	emit_signal("button_slot_pressed", 1)


func _on_Btn_2_pressed() -> void:
	emit_signal("button_slot_pressed", 2)


func _on_Btn_3_pressed() -> void:
	emit_signal("button_slot_pressed", 3)


func _on_Btn_4_pressed() -> void:
	emit_signal("button_wheel_pressed")
