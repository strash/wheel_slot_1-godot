extends TextureRect


var blur = false


func set_blur(param: bool) -> void:
	blur = param
	material.set_shader_param("blur", blur)

func _ready() -> void:
	set_blur(false)
